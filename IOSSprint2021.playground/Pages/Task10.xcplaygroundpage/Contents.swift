//: [Previous](@previous)

import UIKit

/*
 task - 10.1
 - Реалізувати enum Transmission (варіатор, типтронік, робот, ручна)
 - Реалізувати структуру - Car з властивістями модель (String), потужність (Int), коробка передач (Transmission).
 - Додати пропертю description за допомогою розширення структури Car протоколом CustomStringConvertible.
 - Реалізувати фукцію яка буде приймати массив з машинами і повертати масив масивів з автомобілями ([[Car]]) групованих по типу коробки передач
 - Створіть массив з машинами в яких різні коробки передач.
 - Передайте масив в створену Вами функцію і виведіть результат з використанням змінної description
 */

enum Transmission: CaseIterable {
    case variator
    case tiptronic
    case robot
    case mechanic
}

struct Car {
    var model: String
    var enginePower: Int
    var transmission: Transmission
}

extension Car: CustomStringConvertible {
    var description: String {
        return "Model - \(self.model), engine power - \(self.enginePower), transmission - \(String(describing: self.transmission))"
    }
}

func sortedCarOnTransmission(with listCars: [Car]) -> [[Car]] {
    var newArray = [[Car]]()
    
    for transmission in Transmission.allCases {
        let list = listCars.filter { car in
            car.transmission.hashValue == transmission.hashValue
        }
        newArray.append(list)
    }
    return newArray
}

let listCar = [
    Car(model: "Prado", enginePower: 80, transmission: .tiptronic),
    Car(model: "Mark", enginePower: 105, transmission: .mechanic),
    Car(model: "Corolla", enginePower: 95, transmission: .variator),
    Car(model: "Camry", enginePower: 55, transmission: .tiptronic),
    Car(model: "Rav4", enginePower: 145, transmission: .robot),
    Car(model: "Impala", enginePower: 210, transmission: .mechanic),
    Car(model: "Hetz", enginePower: 40, transmission: .variator),
    Car(model: "Camaro", enginePower: 320, transmission: .mechanic),
    Car(model: "Priora", enginePower: 70, transmission: .robot)
]

sortedCarOnTransmission(with: listCar).description

/*
 task - 10.2
 - Додати екстеншен для Transmission в якому іплементувати протокол Hashable
 - Реалізувати в екстеншені массиву функцію group(by: (element: Element) -> AnyHashable) -> [[Element]]
 - Створіть массив з машинами в яких різні коробки передач(Можна використати масив з 1-го завдання)
 - Визвіть на цьому массиві свою функцію і в неї в якості аргументу передайте кложуру яка при отриманні елементу (Машини) буде повертати хеш для коробки передач
 - Виведіть результат з використанням змінної description
 */

extension Transmission: Hashable { }

extension Array {
    func group(by: (_ element: Element) -> AnyHashable) -> [[Element]] {
        
        var array = [[Element]]()
        
        for item in Transmission.allCases {
            var arr = [Element]()
            for element in self where by(element).hashValue == item.hashValue {
                arr.append(element)
            }
            array.append(arr)
        }

        return array
    }
}

let newListCar = listCar
newListCar.group { car in
    return car.transmission
}.description

//: [Next](@next)
