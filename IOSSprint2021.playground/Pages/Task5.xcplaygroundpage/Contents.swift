//: [Previous](@previous)

import Foundation

/*
 Task 5
 - 1 масиву чисел вивести тільки парні (ex: [1, 2, 12, 5, 7, 88])
 - 2.Обчислити суму всіх чисел з масиву (ex: [1, 2, 12, 5, 7, 88])
 - 3.Конвертувати масив [Strings] у масив [Int] (ex: ["1", "2", "12", "bla", "5", "7", "88"]) -> ([])
 */

// MARK task 5 part 1
let numbersArrayOne = [1, 2, 12, 5, 7, 88]
print("is multiple numbers - \(numbersArrayOne.filter { $0.isMultiple(of: 2) })")

// MARK task 5 part 2
let numbersArrayTwo = [1, 2, 12, 5, 7, 88]
print("sum = \(numbersArrayTwo.reduce (0, +))")

// MARK task 5 part 3
let numbersStringArray = ["1", "2", "12", "bla", "5", "7", "88"]
print("is integer numbers - \(numbersStringArray.compactMap { Int($0) })")

//: [Next](@next)
