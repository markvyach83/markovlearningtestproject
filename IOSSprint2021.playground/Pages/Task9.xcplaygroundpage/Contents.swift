//: [Previous](@previous)

import Foundation

// MARK: Task 9.1
/*
 1. Создать структуру ​Employee​ с переменными f​irstName, l​astName​ тип String.
 Создать структуру ​Product​ с переменной ​name​ тип String.
 Добавить если необходимо инициализаторы.
 */

struct Employee {
    var firstName: String
    var lastName: String
    
    init() {
        firstName = ""
        lastName = ""
    }
    
    init(firstName: String, lastName: String) {
        self.firstName = firstName
        self.lastName = lastName
    }
}

struct Product {
    var name: String
    
    init() {
        name = ""
    }
    
    init(name: String) {
        self.name = name
    }
}

// MARK: Task 9.2
/*
 2. Создать класс ​Company​ с переменными:
 - l​ist​ тип [Employee]
 - ​name​ тип String
 Также в класс добавить функцию, которая возвращает Product.
 */
// MARK: Task 9.3
/*
 3. В классе ​Company ​необходимо реализовать 3 инициализатора:
 - required init()
 - с параметрами list: [Employee], name: String, который присваивает значение параметров переменным l​ist​ и ​name
 - сonvenience failable с параметрами employee: Employee?, name: String, который проверяет или параметр employee имеет значение и делегирует выполнение предыдущему инициализатору.
 */

class Company {
    var list: [Employee]
    var name: String
    
    init() {
        name = ""
        list = []
    }
    
    init(list: [Employee], name: String) {
        self.name = name
        self.list = list
    }
    
    convenience init?(employee: Employee?, name: String) {
        if let employee = employee {
            self.init(list: [employee], name: name)
        } else {
            return nil
        }
    }
    
    func getProduct() -> Product {
        return Product()
    }
}

// MARK: Task 9.4
/*
 4. Далее необходимо создать класс ​StateRegistry​ с static переменной registeredCompanies​ тип [Company], и двумя функциями:
 - первая добавляет компанию в r​egisteredCompanies,
 - вторая принимает ​name​ как параметр и проверяет наличие компании в r​egisteredCompanies.
 */

class StateRegistry {
    static var registeredCompanies: [Company] = []
    
    func addCompanyToList(at company: Company) {
        StateRegistry.registeredCompanies.append(company)
    }
    
    func isAbsentCompanyByName(name: String) -> Bool {
        return StateRegistry.registeredCompanies.contains { company in
            return company.name == name
        }
    }
}

// MARK: Task 9.5
/*
 5. Также необходимо создать подкласс ​FoodCompany,​ с переменной qualityCertificate​ тип String без дефолтного значения, в классе необходимо реализовать failable инициализатор с параметрами
 employee: (String?, String?), name: String, qualityCertificate: String, который в случае правильных данных(не nil, не пустые строки) делегирует выполнение наверх.
 */

class FoodCompany: Company {
    var qualityCertificate: String
    
    init?(employee: (String?, String?), name: String, qualityCertificate: String) {
        guard let firstName = employee.0,
           let lastName = employee.1,
           !firstName.isEmpty,
           !lastName.isEmpty
        else { return nil }
        self.qualityCertificate = qualityCertificate
        let newEmployee = Employee(firstName: firstName, lastName: lastName)
        super.init(list: [newEmployee], name: name)
    }
}

// MARK: Task 9.6
/*
 6. Далее необходимо создать класс ​Project​ с переменными
 - ​contractor​ тип Company
 - ​name​ тип String
 и failable инициализатор с параметрами (name: String, company: Company), который проверяет наличие компании в r​egisteredCompanies​ и в случае успеха присваивает параметры переменным своего класса.
 */

class Project {
    var contractor​: Company
    var name​: String
    
    init?(name: String, company: Company) {
        let stateRegistry = StateRegistry()
        if stateRegistry.isAbsentCompanyByName(name: name) { return nil }
        self.contractor​ = company
        self.name​ = name
    }
}

// MARK: Task 9.7
/*
 7. После имплементации всего вышеуказанного, необходимо создать экземпляры компаний при помощи каждого инициализатора, "зарегистрировать" их в r​egisteredCompanies,​ и создать инстансы класса Project.
 */

let companyOne = Company()
let companyTwo = Company(list: [Employee()], name: "companyTwo")
let companyThree = Company(list: [Employee(firstName: "Three", lastName: "Company3")], name: "companyThree")
let companyFour = Company(employee: Employee(), name: "companyFour")
let companyFive = Company(employee: Employee(firstName: "Five", lastName: "Company5"), name: "companyFive")

let registerCompany = StateRegistry()
registerCompany.addCompanyToList(at: companyOne)
registerCompany.addCompanyToList(at: companyTwo)
registerCompany.addCompanyToList(at: companyThree)
registerCompany.addCompanyToList(at: companyFour ?? Company())
registerCompany.addCompanyToList(at: companyFive ?? Company())

let projectOne = Project(name: "projectOne", company: companyOne)
let projectTwo = Project(name: "projectTwo", company: companyTwo)
let projectThree = Project(name: "projectThree", company: companyThree)
let projectFour = Project(name: "projectFour", company: companyFour ?? Company())
let projectFive = Project(name: "projectFive", company: companyFive ?? Company())

//: [Next](@next)
