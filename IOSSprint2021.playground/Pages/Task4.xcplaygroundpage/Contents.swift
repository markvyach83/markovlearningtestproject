//: [Previous](@previous)

import Foundation

/*
 Task 41.Реалізувати функцію:
-1. що розвязує квадратне рівняння і повертає розвязки tuple і error enum (якщо дискримінант < 0, значення дискримінанта в помилці як Associated Value) (нам знадобиться protocol Error, throw, try);
 - 2.*(optional) Реалізувати структуру для представлення комплексних чисел;
 - 3.*(optional) Реалізувати оператори для роботи з комплексними числами;
 - 4.*(optional) Функція що розвязує квадратне рівняння(в якості аргументів Double) і повертає розвязки tuple де значення будуть мати тип з попереднього завдання.
 */

// MARK: task 2 part 1
enum DiscriminantError: Error {
    case negative(descriptionError: Double)
}

let a = 1.0
let b = -6.0
let c = 13.0

func calculateDiscriminant() throws -> (rootOne: Double, rootTwo: Double) {
    let discriminant = calculateSquareRoot(a, b, c)
    
    let twoA = 2 * a
    var rootOne = 0.0
    var rootTwo = 0.0
    
    if discriminant >= 0 {
        rootOne = -b + sqrt(discriminant) / twoA
        rootTwo = -b - sqrt(discriminant) / twoA
    }
    
    guard discriminant < 0 else { throw DiscriminantError.negative(descriptionError: sqrt(discriminant)) }
    
    return (rootOne, rootTwo)
}

func calculateSquareRoot(_ a: Double, _ b: Double, _ c: Double) -> Double {
    return b * b - 4 * a * c
}

try calculateDiscriminant()

// MARK: task 2 part 2
struct ComplexNumber {
    let realPart: Double
    let complexPart: Double
    
    func showComplexRoots() {
        print("the first complex root = \(realPart) + \(complexPart)i, the second complex root = \(realPart) - \(complexPart)i ")
    }
}

// MARK: task 2 part 3
indirect enum ComplexOperator {
    case value(String)
    case addition(ComplexOperator, ComplexOperator)
    case subtraction(ComplexOperator, ComplexOperator)
}

func getComplexRoot(_ expression: ComplexOperator) -> String {
    var rootString = ""
    switch expression {
    case let .value(number):
        rootString = number
    case let .addition(left, right):
        rootString = "\(left) + \(right)i"
    case let .subtraction(left, right):
        rootString = "\(left) - \(right)i"
    }
    return rootString
}

// MARK: task 2 part 4
func calculateDiscriminant(_ a: Double, _ b: Double, _ c: Double) -> (String, String) {
    let discriminant = calculateSquareRoot(a, b, c)
    
    let twoA = 2 * a
    var root = ComplexNumber(realPart: 0, complexPart: 0)
    
    let rootOne: ComplexOperator
    let rootTwo: ComplexOperator
    
    if discriminant < 0 {
        root = ComplexNumber(realPart: -b / twoA,
                             complexPart: sqrt(abs(discriminant)) / twoA)
    }
    
    rootOne = .addition(ComplexOperator.value(String(root.realPart)),
                        ComplexOperator.value(String(root.complexPart)))
    rootTwo = .subtraction(ComplexOperator.value(String(root.realPart)),
                           ComplexOperator.value(String(root.complexPart)))
    
      return (getComplexRoot(rootOne), getComplexRoot(rootTwo))
}

calculateDiscriminant(a, b, c)

//: [Next](@next)
