//: [Previous](@previous)

import Foundation

// MARK: Task 2
/*
 We have a 4 lists of students from one group.
 - allStudents = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
 - present on Monday =       [1, 2, 5, 6, 7]
 - present on Tuesday =      [3, 6, 8, 10]
 - present on Wednesday =    [1, 3, 7, 9, 10]
 Print students that attend university:
 - three days,
 - two days,
 - Monday and Wednesday but not Tuesday,
 - missed all classes
 */

let allStudents: Set =         [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
var onMondayStudents: Set =    [1, 2, 5, 6, 7]
let onTuesdayStudents: Set =   [3, 6, 8, 10]
let onWednesdayStudents: Set = [1, 3, 7, 9, 10]

// MARK: students that attend university three days
func attendUniversityThreeDay() {
    let sameStudentsMondayAndTuesday = onMondayStudents.intersection(onTuesdayStudents)
    let sameStudentsMondayAndTuesdayAndWednesday = sameStudentsMondayAndTuesday.intersection(onWednesdayStudents)
    displayResult(with: "three days", at: sameStudentsMondayAndTuesdayAndWednesday)
}

// MARK: students that attend university Monday and Wednesday but not Tuesday
func attendUniversityMondayAndWednesday() {
    let resultStudent = onMondayStudents.intersection(onWednesdayStudents)
    displayResult(with: "Monday and Wednesday but not Tuesday,", at: resultStudent)
}

// MARK: students that attend university two days
func attendUniversityTwoDay() {
    let sameOne = onMondayStudents
        .intersection(onTuesdayStudents)
        .subtracting(onWednesdayStudents)
    let sameTwo = onTuesdayStudents
        .intersection(onWednesdayStudents)
        .subtracting(onMondayStudents)
    let sameThree = onMondayStudents
        .intersection(onWednesdayStudents)
        .subtracting(onTuesdayStudents)
    let unionStudents = sameOne.union(sameTwo).union(sameThree)
    
    displayResult(with: "two days,", at: unionStudents)
}

// MARK: students that missed all classes
func noAttendUniversity() {
    let unionThreeDays = onMondayStudents
        .union(onTuesdayStudents)
        .union(onWednesdayStudents)
    let difference = allStudents.symmetricDifference(unionThreeDays)
    
    difference.isEmpty
        ? print("All students attended university")
        : difference.forEach { item in
            print("The University missed students № - \(item)")
        }
}

func displayResult(with message: String = "", at array: Set<Int>) {
    array.isEmpty
        ? print("No one attends university \(message)")
        : array.forEach { item in
            print("The University was attended, \(message) by students № - \(item)")
        }
}


//attendUniversityThreeDay()
//attendUniversityTwoDay()
//attendUniversityMondayAndWednesday()
//noAttendUniversity()

//: [Next](@next)
