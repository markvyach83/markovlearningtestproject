//: [Previous](@previous)

import Foundation

/*
 Task 6
 - 1.Реалізувати свій Optional(MyOptional).
 - 2.Реалізувати функцію,яка в якості аргументів буде приймати 2 числа типу MyOptional<Int> і повертати суму цих чисел також типу MyOptional<Int>.
 */

enum MyOptional<T> {
    case none
    case value(T)
    
    var getValue: T {
        switch self {
        case .value(let value):
            return value
        default:
            return "value is not exist" as! T
        }
    }
}

let optionalOne = MyOptional.value(10)
let optionalTwo: MyOptional<Int> = .value(4)

func calculateSumTwoArguments<T: Numeric>(_ left: MyOptional<T>, _ right: MyOptional<T>) -> MyOptional<T> {
    return MyOptional.value((left.getValue + right.getValue))
}

calculateSumTwoArguments(optionalOne, optionalTwo)

//: [Next](@next)
