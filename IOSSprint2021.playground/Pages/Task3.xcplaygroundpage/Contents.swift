//: [Previous](@previous)

import Foundation

// MARK: Task 3
/*
 3.1.Пройтись по масиву використовуючи for-in, while, repeat-while і вивести всі величини.
 3.2.Пройтись по масиву використовуючи for-in, while, repeat-while і вивести всі парні величини, а потім всі непарні значення.
 */

let numbersArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

func isMultipleForIn(array: [Int], isMultiple: Bool?) {
    for item in array {
        if item.isMultiple(of: 2) == isMultiple {
            print(item)
        } else if isMultiple == nil {
            print(item)
        }
    }
}

func isMultipleWhile(array: [Int], isMultiple: Bool?) {
    var count = 0
    while count < array.count {
        if array[count].isMultiple(of: 2) == isMultiple {
            print(array[count])
        } else if isMultiple == nil {
            print(array[count])
        }
        count += 1
    }
}

func isMultipleRepeadWhile(array: [Int], isMultiple: Bool?) {
    var count = 0
    repeat {
        if array[count].isMultiple(of: 2) == isMultiple {
            print(array[count])
        } else if isMultiple == nil {
            print(array[count])
        }
        count += 1
        
    } while count < array.count
}

//MARK: Print all cases
//isMultipleForIn(array: numbersArray, isMultiple: nil)
//isMultipleWhile(array: numbersArray, isMultiple: nil)
//isMultipleRepeadWhile(array: numbersArray, isMultiple: nil)

//MARK: Print pair cases
//isMultipleForIn(array: numbersArray, isMultiple: true)
//isMultipleWhile(array: numbersArray, isMultiple: true)
//isMultipleRepeadWhile(array: numbersArray, isMultiple: true)

//MARK: Print unpaired cases
//isMultipleForIn(array: numbersArray, isMultiple: false)
//isMultipleWhile(array: numbersArray, isMultiple: false)
//isMultipleRepeadWhile(array: numbersArray, isMultiple: false)

//: [Next](@next)
