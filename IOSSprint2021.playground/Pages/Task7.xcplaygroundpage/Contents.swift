//: [Previous](@previous)

import Foundation

/*
 Task 7
 Використовуючи Enum реалізувати LinkedList Методи:
 - 1. append(value : T) - додати в кінець
 - 2. description - повертає строку зі всіма значеннями починаючи з голови.
 */

indirect enum LinkedList<T> {
    case value(element: T, LinkedList<T>)
    case end
}


extension LinkedList {
    func append(value: T) -> LinkedList {
        return .value(element: value, self)
    }
    
    func description() {
        var temp = self
        while case let LinkedList.value(element: value, next) = temp {
            print(value)
            temp = next
        }
    }
}

var list: LinkedList = .value(element: 1, .end).append(value: 2).append(value: 3)
list.description()

//: [Next](@next)
