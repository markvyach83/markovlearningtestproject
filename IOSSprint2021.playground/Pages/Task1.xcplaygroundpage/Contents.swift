import UIKit

/*
 Практичне завдання:
 task 1 - Оголосити декілька змінних типу Int обчисли: середнє арифметичне, геометричне.
 
 task 2 - Оголосити змінні для коефіцієнтів квадритного рівняння (a, b, c) типу Double, обчисли корені рівняння. Використовуючи функцію print() вивести корені у вигляді: "a*x^2 + b*x + c = 0, root1 =1 , root2=2 "
 */


// MARK task 1
let numbersArray = [3, 6, 9]

func calculateAverageOfGeometric(with array: [Int]) -> Double? {
    var sum = 1.0
    for item in array {
        sum *= Double(item)
    }
    return pow(sum, 1.0 / Double(array.count))
}

func calculateAverageOfArithmetic(with array: [Int]) -> Double {
    var sum = 0
    for item in array {
        sum += item
    }
    return Double(sum) / Double(array.count)
}

calculateAverageOfGeometric(with: numbersArray)
calculateAverageOfArithmetic(with: numbersArray)

// MARK task 2
let a = 1.0
let b = -6.0
let c = 13.0

func calculateSquareRoot(a: Double, b: Double, c: Double) {
    
    let discriminant: Double = b * b - 4 * a * c
    var rootOne = ""
    var rootTwo = ""
    
    let twoA = 2 * a
    
    if discriminant > 0 {
        rootOne = "\((-b + sqrt(discriminant)) / (twoA))"
        rootTwo = "\((-b - sqrt(discriminant)) / (twoA))"
        
    } else if discriminant == 0 {
        rootOne = "\(-b / (twoA))"
        rootTwo = rootOne
        
    } else {
        let discriminantAbs = abs(discriminant)
        
        rootOne = "\( -b / twoA) + \(sqrt(discriminantAbs) / twoA)i"

        rootTwo = "\( -b / twoA) - \(sqrt(discriminantAbs) / twoA)i"
    }
    
    print("\(a)a*x^2 + \(b)*x + \(c) = 0, root1 = \(rootOne), root2 = \(rootTwo)")
}

calculateSquareRoot(a: a, b: b, c: c)
